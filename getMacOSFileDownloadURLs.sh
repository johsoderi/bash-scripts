#!/usr/bin/env bash

# NAME:  getMacOSFileDownloadURLs.sh

# DESCR: A tool for getting "quarantine" info for a macOS file, such as the URL from which it was downloaded.
# The macOS Finder info box will sometimes show you the URL of a downloaded file, but not always.
# I noticed a file lacking such info, although macOS apparently knew exactly where it came from (it told me
# in one of those lovely "X can't be opened because Apple cannot check it for malicious software" dialogs.)
# If such information is available I want a quick way get my hands on it, so I wrote this script.

# USAGE: ./getMacOSFileDownloadURLs.sh <PATH>

# EXAMPLE: $ ./getMacOSFileDownloadURLs.sh ~/Library/QuickLook/qlImageSize.qlgenerator
#   Filename: qlImageSize.qlgenerator
#   TimeStamp: 2020-10-07 11:48:56
#   EventIdentifier: DA0BC88A-634F-4703-B1D0-CE420CC8D6B6
#   AgentBundleIdentifier:
#   AgentName: Homebrew Cask
#   DataURLString: https://github.com/Nyx0uf/qlImageSize/releases/download/2.6.1/qlImageSize.qlgenerator.zip
#   SenderName:
#   SenderAddress:
#   TypeNumber: 0
#   OriginTitle:
#   OriginURLString: https://github.com/Nyx0uf/qlImageSize
#   OriginAlias:

# If you get an error, try changing "QuarantineEventsV2" into "QuarantineEventsV*".
quarantineEventsDBpath=~/Library/Preferences/com.apple.LaunchServices.QuarantineEventsV2

# First get the relevant xattributes for the file:
xattrStr="$(xattr -p com.apple.quarantine "$1")"

# Slice it up to get the EventIdentifier, accounting for encoding shenanigans:
fileId=$(LC_CTYPE=C && LANG=C && echo $xattrStr | cut -d';' -f4)

# Use this ID to query the database containing the info:
answer=$(sqlite3 "$quarantineEventsDBpath" "SELECT * FROM LSQuarantineEvent WHERE LSQuarantineEventIdentifier=\"${fileId}\"")

# Fetch the timestamp separately to get a human-readable datetime:
TimeStamp=$(sqlite3 "$quarantineEventsDBpath" "SELECT datetime(LSQuarantineTimeStamp + 978307200, 'unixepoch', 'localtime') FROM LSQuarantineEvent WHERE LSQuarantineEventIdentifier=\"${fileId}\"")

echo ""
echo "Filename: "$(basename $1)""
echo "TimeStamp: $TimeStamp"
echo "EventIdentifier: $(echo "$answer" | cut -d'|' -f 1)"
echo "AgentBundleIdentifier: $(echo "$answer" | cut -d'|' -f 3)"
echo "AgentName: $(echo "$answer" | cut -d'|' -f 4)"
echo "DataURLString: $(echo "$answer" | cut -d'|' -f 5)"
echo "SenderName: $(echo "$answer" | cut -d'|' -f 6)"
echo "SenderAddress: $(echo "$answer" | cut -d'|' -f 7)"
echo "TypeNumber: $(echo "$answer" | cut -d'|' -f 8)"
echo "OriginTitle: $(echo "$answer" | cut -d'|' -f 9)"
echo "OriginURLString: $(echo "$answer" | cut -d'|' -f 10)"
echo "OriginAlias: $(echo "$answer" | cut -d'|' -f 11)"
