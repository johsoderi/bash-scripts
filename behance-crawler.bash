#!/usr/bin/env bash
#title:        behance-crawler.bash
#description:  Crawls a behance.net user page and downloads the cover photo of every project.
#version:      1.0
#date:         2021-05-23
#author:       johsoderi
#dependencies: Parses html with pup (https://github.com/ericchiang/pup) and json with jq (https://stedolan.github.io/jq/).
#output:       username/FirstnameLastname-ProjectName.jpg
#usage:        [chmod +x] ./behance-crawler.bash <username>

save_to_dir="/path/to/parent/dir/"

if [ "$#" -ne 1 ] ; then
  echo "Missing argument <username>"
  exit 1
fi

mkdir "$save_to_dir""$1"

offset=0
title="x"

while [ ! -z $title ] ; do
  data="$(curl -s https://www.behance.net/$1/projects?offset=$offset)"
  echo "Offset $offset:"
  for i in {0..11} ; do
    artist="$(echo $data | pup 'script' | head -14 | tail -1 | jq -r ".[] | .activeSection | .work | .projects | .[$i] | .owners | .[0] .display_name" | sed 's/null//g' | sed 's/\ //g' | sed '/^$/d')"
    image_url="$(echo $data | pup 'script' | head -14 | tail -1 | jq -r ".[] | .activeSection | .work | .projects | .[$i] | .covers | .original" | sed 's/null//g' | sed '/^$/d')"
    title="$(echo $data | pup 'script' | head -14 | tail -1 | jq -r ".[] | .activeSection | .work | .projects | .[$i] .name" | sed 's/null//g' | sed 's/\ //g' | sed '/^$/d')"
    filename="$artist-$title.jpg"
    if [ ! -z $title ] ; then
      echo "$image_url -> $filename"
      curl -s "$image_url" > "$save_to_dir$1/$filename"
    fi
  done
  offset=$(($offset + 12))
done
