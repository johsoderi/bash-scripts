#!/usr/bin/env bash
# ifconfig in macOS outputs subnet mask in hex. This script converts it into decimal format.

list="$(ifconfig)"
while IFS= read -r line; do
	if [[ "$(echo "$line" | grep "netmask 0x")" ]] ; then
		ip_addr="$(echo "$line" | awk '{for(i=1;i<=NF;i++) if ($i=="inet") print $(i+1)}')"
		netmask_hex="$(echo "$line" | awk '{for(i=1;i<=NF;i++) if ($i=="netmask") print $(i+1)}')"
		oct_1="$(printf "%d\n" 0x"$(echo ${netmask_hex:2:2})")"
		oct_2="$(printf "%d\n" 0x"$(echo ${netmask_hex:4:2})")"
		oct_3="$(printf "%d\n" 0x"$(echo ${netmask_hex:6:2})")"
		oct_4="$(printf "%d\n" 0x"$(echo ${netmask_hex:8:2})")"
		netmask_dec="$(echo "$oct_1.$oct_2.$oct_3.$oct_4")"
	fi
	echo "$line" | sed "s/$netmask_hex/$netmask_dec/g"
done <<< "$list"
