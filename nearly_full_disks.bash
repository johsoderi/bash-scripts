#!/usr/bin/env bash
#title:       nearly_full_disks.bash
#description: Lists volumes that are nearly full
#author:      Johannes Söderberg Eriksson - DevOps18
#date:        2019-10-09
#usage:       [chmod u+x] ./nearly_full_disks.bash [<threshold>]

do_the_thing () {
	LIMIT="$1"
	# Lägg outputen från df i en variabel, så vi slipper köra det flera ggr:
	LIST=$(df --output=pcent,target)
	# Ta bort första raden (rubrikerna) från underlaget:
	LIST=`echo "${LIST}" | sed 1d`
	# Plocka fram de enheter som är "nästan fulla" enligt angivet gränsvärde:
	echo "$LIST" | sed 's/%//g' | awk -v lim="$LIMIT" '{if ($1>=lim) print}'\
	       	| sed 's/\ \//%\ \//g'
	# Förklaring till de olika delarna:
	# sed 's/%//g'		Ta bort %-tecken, så vi kan räkna med siffrorna
	# awk -v lim="$LIMIT"	Skicka in skal-variabeln $LIMIT i awk-variabeln lim
	# {if ($1>=lim) print}	Skriv ut raden, ifall fält 1 = lim eller större
	# sed 's/\ \//%\ \//g'	Stoppa tillbaka %-tecken
}

# Agera beroende på antal argument:
case "$#" in    
	# Inga argument, anropa do_the_thing med default-värde 90%:
	(0)	do_the_thing 90
		;; 		
	# Ett argument, kolla ifall $1 är ett positivt heltal genom att testa om 
	# det är större än -1, och skicka ev. felmeddelanden åt fanders:
	(1)	if [ $1 -gt -1 > /dev/null 2>&1 ] ; then 
			# Heltal, anropa do_the_thing med första arg. som värde:
			do_the_thing "$1"
		else
			# Ej heltal, ge felmeddelande och avsluta med status 1:
			echo "expected an integer" 1>&2
			exit 1
		fi
		;;
	# Två eller flera argument, ge felmeddelande och avsluta med status 1:
	(*)	echo "expected an integer" 1>&2
       		exit 1
		;;
esac
