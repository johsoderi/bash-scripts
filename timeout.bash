#!/usr/bin/env bash
#title:       timeout.bash 
#description: Run a command with a timeout
#author:      Johannes Söderberg Eriksson - DevOps18 
#date:        2019-10-16 
#usage:       [chmod u+x] ./timeout.bash <time limit in seconds> <command> [<args>]... 
#version:     0.02a 

WAITFOR="$1"
NUM='^[0-9]+$'
USAGE="Usage: ./timeout.bash <time limit in seconds> <command> [<args>]..."

outtatime(){
	echo "TIMEOUT"
	exit 1
}

case $# in
	# Om antal argument är 0 eller 1, visa hjälp och avsluta med status 1:
	(0 | 1) echo "$USAGE" ; exit 1 ;;
	# Om antal argument är fler än 1, men arg 1 inte är en siffra:
	(*) if ! [[ $WAITFOR =~ $NUM ]] ; then #> /dev/null 2>&1 ]] ; then
                echo "$USAGE" ; exit 1
	    # Om allt verkar stämma, kör kommandot med argument:
            else
		shift
		CMD="$@"
		($CMD) &
		CMD_PID=$!
		export SCRIPT_PID=$$
	    fi ;;
esac

# Fånga upp signal från timer-processen:
trap outtatime SIGUSR1

# Starta timer, och om tiden tar slut, skicka SIGUSR1 till scriptets process:
(sleep "$WAITFOR" ; kill -SIGUSR1 "$SCRIPT_PID") &
TIMER_PID=$!
 
# Vänta på att kommandot körs:
wait "$CMD_PID"

# Avsluta:
echo "SLUTSTATUS: $?"
kill "$TIMER_PID"
