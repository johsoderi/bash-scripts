#/usr/bin/env bash

#title:       vagrant_mac_deploy_webserver.bash
#description: Installs Vagrant under macOS and automatically deploys a VM in current dir
#author:      Johannes Söderberg Eriksson - DevOps18
#date:        2019-10-17
#usage:       [chmod u+x] ./vagrant_mac_deploy_webserver.bash
#version:     0.25

#  Installera Vagrant - macOS/Homebrew/virtualbox
	brew update
	brew cask install virtualbox
	brew cask install vagrant
	brew cask install vagrant-manager

# [Vagrantfile]
cat > Vagrantfile << EOT
Vagrant.configure("2") do |config|
  config.vm.box = "walkeymj/ubuntu-bionic-server-LAMP"
  config.vm.box_version = "1.0.0"
  config.vm.provider :virtualbox do |v|
    v.memory = 4096
    v.cpus = 2
  end
  config.vm.network :private_network, ip: "192.168.100.100"
  config.vm.network :forwarded_port, guest: 80, host: 8080
  config.vm.provision :shell, path: "setup.sh"
  config.vm.provision "file", source: "index.php", destination: "/vagrant/html/index.php"
  config.vm.provision "file", source: "setup_db.sql", destination: "/home/vagrant/setup_db.sql"
end
EOT

# [setup.sh] Bash-skript som kallas från Vagrantfile
cat > setup.sh << EOT
#!/usr/bin/env bash
 
echo "Exekverar setup.sh..."  
if ! [ -L /var/www ]; then
  rm -rf /var/www
  ln -fs /vagrant /var/www
fi
sudo mysql --defaults-extra-file=/etc/mysql/debian.cnf < /vagrant/setup_db.sql
EOT

# [setup_db.sql] mySQL-script som skapar en ny user "php" samt en databas med en tabell
cat > setup_db.sql << EOT
create user 'php'@'localhost' identified by 'vagrant';
grant all privileges on *.* to 'php'@'localhost' with grant option;
flush privileges;
CREATE DATABASE WebbApp;
USE WebbApp ;
CREATE TABLE Information (
 VisitId INT NOT NULL AUTO_INCREMENT,
 VisitDate TIMESTAMP,
 VisitorIP VARCHAR(45),
 PRIMARY KEY (VisitId));
EOT

# [index.php] PHP-sida som registrerar klientens IP-nr och visar besökshistorik
IFS='' read -r -d '' String <<"EOF"
<!DOCTYPE html>
<html>
 <head>
  <title>joso1801 - DevOps18</title>
 </head>
 <body>
  <?php
    $servername = "localhost";
    $username = "php";
    $password = "vagrant";
    $dbname = "WebbApp";
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
    }
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
      $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
      $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
      $ip = $_SERVER['REMOTE_ADDR'];
    }
    $VisitDate = date('Y-m-d H:i:s');
    $sql = "INSERT INTO Information (VisitorIP, VisitDate)
            VALUES ('$ip', '$VisitDate')";
    if ($conn->query($sql) === TRUE) {
      echo "<br><h1>DEVOPS18 keeps track of things!</h1><br>";
    } else {
      echo "FEL FEL FEL: " . $sql . "<br>" . $conn->error;
    }
    $rows=$conn->query("select VisitorIP, VisitDate from Information");
    echo "<table border='1'>";
    echo "<tr><th>IP</th><th>Tidpunkt (UTC)</th></tr>";
    while(list($VisitorIP, $VisitDate)=$rows->fetch_row()){
      echo "<tr><td>$VisitorIP</td><td>$VisitDate</td></tr>";
    }
    echo "</table>";
    $conn->close();
  ?>
 </body>
</html>
EOF
echo "${String}" > index.php

# Döda ev. exiserande instans och städa upp
sudo kill -9 $(sudo lsof -nP -i4TCP:8080 | grep VBoxHeadl | awk '{print $2}')
sudo vagrant destroy -f || true

# Sätt upp VM
sudo vagrant up

