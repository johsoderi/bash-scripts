#!/usr/bin/env bash
#title:       install_curl.sh 
#description: Download, compile och install curl under /opt. 
#author:      Johannes Söderberg Eriksson - DevOps18
#date:        2019-10-08
#usage:       [chmod u+x] ./install_curl.sh [-v --verbose] [-c --clean] [-h --help]
#version:     0.03a

SERV="https://curl.haxx.se/download/"
NAME="curl-7.61.1"
SUFF="tar.bz2"
TAR_PARAM="xjvf"
VERBOSE="0"
FULL_CMD=

clean () {
	echo "Raderar loggar och uppackade filer..."
	rm -rf /tmp/fixa_curl_*.log
	rm -rf ./curl-7.61.1
	echo "Klart, programmet avslutas..."
	exit 0
}

display_help () {
	echo "Usage:\n./install_curl.sh [-v --verbose] [-c --clean] [-h --help]"
	exit 0
}

while [ "$#" -gt 0 ] ; do
	case "$1" in
		(-c       ) clean        ;;
		(--clean  ) clean        ;;
		(-h       ) display_help ;;
		(--help   ) display_help ;;
		(-v       ) VERBOSE="1"  ;;
		(--verbose) VERBOSE="1"  ;;
		(*        ) display_help ;;
	esac
	shift 1
done

check_verbose () {
	case "$VERBOSE" in
		(0) FULL_CMD="$1 >>/tmp/fixa_curl_$2.log 2>&1" ;;
		(1) FULL_CMD="$1" ;;
		(*) return 1;;
	esac
	eval "$FULL_CMD"
	return "$?"
}

err_check () {
	if [ ! "$1" -eq 0 ] ; then
		echo "$2 avslutades med felkod $1."
		echo "Se /tmp/fixa_curl_$2.log för mer information."
		echo "För att radera loggar mm, kör:"
		echo "$ ./fixa_curl.sh --clean"
		echo "Avslutar..."
		exit 1
	else
		return
	fi
}

/usr/bin/clear

echo "Letar efter $NAME.$SUFF i aktuell katalog..."
if [ ! -f "./$NAME.$SUFF" ] ; then
	TASK="curl"
	echo "Filen hittades inte. Hämtar nu..."
	check_verbose "curl --output ./$NAME.$SUFF $SERV$NAME.$SUFF $REDIR" "$TASK"
	err_check "$?" "$TASK"
	echo "Klart."
else
	echo "Filen hittades."
fi

TASK="tar"
echo "Packar upp källkod..."
check_verbose "tar -$TAR_PARAM ./$NAME.$SUFF" "$TASK"
err_check "$?" "$TASK"
echo "Klart."

TASK="cd"
echo "Exekverar:		cd $NAME"
check_verbose "cd $NAME" "$TASK"
err_check "$?" "$TASK"
echo "Klart."

TASK="configure"
echo "Exekverar:		./configure --prefix=/opt"
check_verbose "./configure --prefix=/opt" "$TASK"
err_check "$?" "$TASK"
echo "Klart."

TASK="make"
echo "Exekverar:		make"
check_verbose "make" "$TASK"
err_check "$?" "$TASK"
echo "Klart."

TASK="make_check"
echo "Exekverar:		make check"
check_verbose "make check" "$TASK"
err_check "$?" "$TASK"
echo "Klart."

TASK="make_install"
echo "Exekverar:		make install"
check_verbose "make install" "$TASK"
err_check "$?" "$TASK"
echo "Klart."

TASK="cd"
echo "Exekverar:                cd .."
check_verbose "cd .." "$TASK"
err_check "$?" "$TASK"
echo "Klart."
echo "$NAME är installerat."

clean

