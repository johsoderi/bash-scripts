#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Name:     coords2stations.py
# Author:   johsoderi
# Date:     2023-02-20
# Desc:     Feed it coordinates and get back a list of the radio stations closest to the location. Uses radio.garden's API.
# Usage:    ./coords2stations.py LATITUDE LONGITUDE
# Example:  ./coords2stations.py -73.25 68.383

import sys
import urllib.request
import json
from math import cos, asin, sqrt

if (len(sys.argv) != 3):
    print("Needs 2 arguments: latitude longitude")
    sys.exit(1)

latInput = float(sys.argv[1])
longInput = float(sys.argv[2])
baseUrl = "http://radio.garden/api/ara/content"

def distance(lat1, lon1, lat2, lon2):
    p = 0.017453292519943295
    a = 0.5 - cos((lat2-lat1)*p)/2 + cos(lat1*p)*cos(lat2*p) * (1-cos((lon2-lon1)*p)) / 2
    return 12742 * asin(sqrt(a))

def gibClosest(v):
        return min(placeList, key=lambda p: distance(v['lat'],v['lon'],p['geo'][1],p['geo'][0]))

with urllib.request.urlopen(baseUrl + '/places') as response:
    placeList = json.loads(response.read())['data']['list']

closestId = gibClosest({'lat':latInput, 'lon':longInput})['id']

# with urllib.request.urlopen(baseUrl + '/page/' + closestId) as response:
#     placeData = json.loads(response.read())
#     place = placeData['data']['title']
#     country = placeData['data']['subtitle']
#     print("Place: " + place + ", " + country)

with urllib.request.urlopen(baseUrl + '/page/' + closestId + "/channels") as response:
    channelsData = json.loads(response.read())

channelList = []
for channel in channelsData['data']['content'][0]['items']:
    channelName = channel['title']
    channelId = channel['href'].split('/')[-1]
    listenUrl = baseUrl + "/listen/" + channelId + "/channel.mp3"
    infoUrl = baseUrl + "/channel/" + channelId
    channelList.append({'name':channelName, 'listen_url':listenUrl, 'info_url':infoUrl})

channelListJson = json.dumps(channelList, indent=2)
print(channelListJson)
