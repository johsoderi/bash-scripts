#!/usr/bin/env bash
#title:       print_tree.bash
#description: Feed it a path and get a file tree
#author:      Johannes Söderberg Eriksson - DevOps18
#date:        2019-10-12
#usage:       [chmod u+x] ./print_tree.bash <PATH>
#version:     0.11b

if [ "$#" -ne 1 ] ; then
	exit 1
fi

ARGPATH="$1"
# För att slippa krångla med cd-kommandon jobbar skriptet med absoluta sökvägar
SCRIPTPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
THEPATH=
LEVEL=0
ITEM="$(basename "$ARGPATH")"

# Ta bort ev. snedstreck i slutet av sökvägen
if [ $(echo "$ARGPATH" | rev | cut -b 1 ) == "/" ] ; then
	ARGPATH="${ARGPATH%?}"
fi

# Ifall sökvägen som ges är absolut ska den inte ändras
if [ $(echo "$ARGPATH" | cut -b 1) == "/" ] ; then
	THEPATH="$ARGPATH"
else
	# Men om den är relativ så gör vi om den till absolut
	THEPATH="$SCRIPTPATH/$ARGPATH"
fi

dots () {
# Denna funktion skriver ut tre punkter för varje katalog-nivå.
	for i in $( seq 0 $LEVEL )
	do
		printf "..."
	done
}

buildtree () {
# Denna funktion läser output från "ls -hl <sökväg>" och behandlar datan.
	while IFS= read -r LINE
	do
		# Först kollar vi ifall ls misslyckades pga otillräckliga rättigh.
		if  cat /tmp/lista_error | grep Permission > /dev/null 2>&1 ; then
			dots; echo "...???"
			rm /tmp/lista_error 2>/dev/null
		fi

		# Ignorera första raden av outputen från ls
		if [ $(echo "$LINE" | awk '{print $1}') == "total" ] ; then
			continue
		fi
		
		# Spara filnamnet och filtypen från ls:s output
		ITEM="$(echo "$LINE" | awk '{print $9}')"
		TYPE="$(echo "$LINE" | cut -b 1)"
		
		case "$TYPE" in
			(-) dots; echo "[F]$ITEM" ;;
			(l) dots; echo "[L]$ITEM" ;;
			(d) dots; echo "[D]$ITEM"
			# Om det är en katalog: Öka $LEVEL (som styr hur många
			# punkter som skrivs ut), lägg till namnet efter sökvägen,
			# kör denna funktion igen, ändra tillbaka och gå vidare
			    ((LEVEL+=1))
			    THEPATH="$THEPATH/$ITEM"
			    buildtree
			    (( LEVEL -= 1 ))
			    THEPATH=$(dirname "$THEPATH") ;;
			# Specialfiler hanteras som vanliga filer.
			(*)	dots; echo "[F]$ITEM" ;;
		esac
	done < <(ls -hl "$THEPATH"  2>/tmp/lista_error)
}

# Kolla om sökvägen leder nånstans
if [ -e "$THEPATH" ] ; then
	# Spara filtypen i en variabel
	POST_TYPE="$(find $THEPATH -maxdepth 0 -ls | awk '{print $3}' | cut -c1)"
	case "$POST_TYPE" in
		(l) echo "[L]$ITEM"              ;;
		(-) echo "[F]$ITEM"              ;;
		(d) echo "[D]$ITEM";  buildtree  ;;
		(*) echo "[F]$ITEM"              ;;
	esac
else
	echo "[?]$ARGPATH"
fi

# Städa bort ev. spill
rm /tmp/lista_error 2>/dev/null
