#!/usr/bin/env bash
#title:       what.bash
#description: What is this file, and is it part of any package?
#author:      Johannes Söderberg Eriksson - DevOps18
#date:        2019-10-05
#usage:       [chmod u+x] ./what.bash {is | has} <file> [<file 2>]...
#version:     100.1

EXITCODE=0
Q_FLAG=0
POST_TYPE=

# Kontrollera om kommandot anropats med flaggan -q:
if [ "$#" -gt 0 ] && [ "$1" == "-q"  ] ; then
	Q_FLAG=1
	shift 1
fi

# Skriv bara ut till standard output ifall flaggan -q inte är satt:
qecho () {
	if [ "$Q_FLAG" -eq 0 ] ; then
		echo "$1"
	fi
}

# Funktion som itererar igenom alla argument och kontrollerar om motsvarande katalogposter finns,
# samt ifall de är symlänkar, kataloger eller filer:
what_is_it () {
	while [ $# -gt 0 ]
	do
		if [ -e "$1" ] ; then
			POST_TYPE="$(find "$1" -maxdepth 0 -ls | awk '{print $3}' | cut -c1)"
			case "$POST_TYPE" in
				(l) POST_TYPE="link"
				    CDIR=$PWD
				    cd $(dirname $1)
				    cd $(dirname $(readlink $1))
				    FULL_PATH="$PWD/$(basename $1)"
				    POST_TYPE="link to $FULL_PATH"
				    cd $CDIR
				    ;;
				(d) POST_TYPE="directory" ;;
				(-) POST_TYPE="file" ;;
			esac
			qecho "$1 is a $POST_TYPE"
		else
			qecho "$1 does not exist."
			((EXITCODE+=2))
		fi
		shift 1
	done
	return 1
}

# Funktion som itererar igenom alla argument och kontrollerar vilket paket de tillhör (om något):
what_has_it () {
	while [ $# -gt 0 ]
	do
		# Om dpkg avslutas utan felkod, sätt $PKG_NAME
		# till 1:a ordet från sista raden av dess output:
		if dpkg -S "$1" &> /dev/null ; then
			PKG_NAME=$(dpkg -S "$1" | tail -1 | cut -d: -f1)
			qecho "$1 is part of package: $PKG_NAME"

		# Annars, om rpm avslutas utan felkod, sätt $PKG_NAME
		# till dess output:
		elif rpm -qf "$1" &> /dev/null ; then
			PKG_NAME=$(rpm -qf "$1")
			qecho "$1 is part of package: $PKG_NAME"
		# Or if it's a homebrew pkg, check the symlink path for '/cellar/':
		elif BREW=$(brew --cellar) ; then
			PKG_NAME=$(find $BREW -name $(basename $1))
			PKG_NAME=$(echo $PKG_NAME | awk -F'/Cellar/' '{ print $2 }' | cut -d/ -f1)
			if [[ $PKG_NAME != "" ]] ; then
				qecho "$1 is part of package: $PKG_NAME"
			else
				qecho "$1 is not part of any package"
				((EXITCODE+=2))
			fi
		else
			qecho "$1 is not part of any package, or package manager is not supported."
			((EXITCODE+=2))
		fi
		shift 1
	done
	return 0
}

# Funktion som kontrollerar argumenten och avgör hur skriptet ska gå vidare:
check_syntax () {
	case $# in
		0) echo "Hello, yes?" >&2 ; exit 1 ;;
		1)
			if [ "$1" != "is" ] && [ "$1" != "has" ] ; then
				echo "I don't get it." >&2
				((EXITCODE+=1))
			else
				echo "What $1 what?" >&2
				((EXITCODE+=1))
			fi ;;
		*)
			if [ "$1" == "is" ] ; then
				shift 1
				what_is_it "$@"
			elif [ "$1" == "has" ] ; then
				shift 1
				what_has_it "$@"
			fi ;;
	esac
}

check_syntax "$@"
if [ "$EXITCODE" -gt 0 ] ; then
	exit 2
else
	exit 0
fi