#!/usr/local/bin/gsed -rf
#title:       points.sed 
#description: Parse points and names from a file and print them. Assignment in a scripting course.
#author:      Johannes Söderberg Eriksson - DevOps18
#date:        2019-10-09
#usage:       [chmod u+x] ./points.sed [FILE] [ ... ]
#version:     0.01


/^[A-Z][a-z]+[[:space:]]+[A-Z][a-z]+[[:space:]]+[0-9]+$/!d

#  /		Pattern start
#  ^		Indicates the following expression must be in the beginning of a line to match
#  [A-Z] 	One capital letter (square brackets define a range)
#  [a-z]+	One or more lowercase letter(s)
#  [[:space:]]+	One or more whitespace character(s) (space, tab, etc.)
#  [A-Z] 	One capital letter
#  [a-z]+	One or more lowercase letter(s)
#  [[:space:]]+	One or more whitespace character(s)
#  [0-9]+	One or more digits
#  $		Indicates the previous expression must be in the end of a line to match
#  /		Pattern end
#  !d		Delete everything not matching the pattern

s/^(.+)[[:space:]]+(.+)[[:space:]]+(.+)$/\3 \2, \1/g

#  s		Substitution
#  /		Pattern start
#  ^		Indicates the following expression must be in the beginning of a line to match
#  (.+)	 	One or more character(s) - Group 1 (parantheses defines a group)
#  [[:space:]]+	One or more whitespace character(s)
#  (.+)	 	One or more character(s) - Group 2
#  [[:space:]]+	One or more whitespace character(s)
#  $		Indicates the previous expression must be in the end of a line to match
#  /		Pattern end
#  \3 		Prints group 3 (points)
#  \2, 		Prints group 2 (last name)
#  \1 		Prints group 1 (first name)
